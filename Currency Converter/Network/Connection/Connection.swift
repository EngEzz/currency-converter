//
//  Connection.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
class Connection {
    static let shared = Connection()
    private let accessKey = "9d28b67a461c0b90fa03f3224dc885af"
    private let baseUrl = "http://data.fixer.io/api/"
    func retrieveData<T:Decodable>(urlName:String,decoder:T.Type)->Observable<T?>
    {
        return Observable.create{[weak self] observer in
            let url:URL = URL(string: "\(self?.baseUrl ?? "" )\(urlName)\(self?.accessKey ?? "" )")!
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseDecodable(of: decoder){[weak self] response in
                guard let _ = self else {return}
                print("\(urlName):Response:\(String(describing: response.value))")
                guard let data = response.value else {observer.onError(response.error ?? NSError(domain: "Api Failure", code: 1)); return }
                observer.onNext(data)
            }
            return Disposables.create()
        }
    }
}
