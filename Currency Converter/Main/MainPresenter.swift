//
//  MainPresenter.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class MainPresenter {
    var interactor: PresenterToInteractorMainProtocol?
    var router: PresenterToRouterMainProtocol?
    var presenter: InteractorToPresenterMainProtocol?
    weak var view:PresenterToViewMainProtocol?
    var currencies: [String : Double]?
    var baseRate:Double?
    var baseCurrency:String?
    let showBase = 0 , showCalculationVw = 1
    init(view:PresenterToViewMainProtocol,interactor:PresenterToInteractorMainProtocol,router:PresenterToRouterMainProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}
extension MainPresenter:InteractorToPresenterMainProtocol{
    func fetchCurrenciesSuccess(currencies: [String : Double]?) {
        self.currencies = currencies
        self.view?.reloadData()
    }
    func fetchCurrenciesFailure(error:NSError) {
        self.view?.showErrorMessage(error: error)
    }
    func fetchCurrencyRate(with tag : Int ,for currency :String,currencyRate: Double) {
        if tag == showBase {
            self.baseRate = currencyRate
            self.baseCurrency = currency
            view?.showCurrency(currency: currency)
        }
        else
        {
            view?.calculateConversion(with : currency , with:currencyRate )
        }
    }
}
extension MainPresenter:ViewToPresenterMainProtocol{
    func showCalculationDialog(with currency : String , with rate:Double) {
        router?.showCalculationDialog(selectedCurrencies: ["baseCurrency":baseCurrency!  ,"baseRate":baseRate! ,"userCurrency":currency,"userCurrencyRate":rate], currencies: currencies, view: view)
    }
    func viewDidLoad() {
        self.interactor?.getCurrencies()
    }
    func selectCurrency(tag: Int) {
        router?.showCurrencyDialog(tag: tag, currencies: currencies, view: view)
    }
    func setSelectedCurrency(with tag : Int ,currency: String) {
        interactor?.getCurrencyBase(with : tag,currency: currency)
    }
}
