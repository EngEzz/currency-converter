//
//  FibonacciTest.swift
//  FibonacciTest
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import XCTest
@testable import Currency_Converter
class FibonacciTest: XCTestCase {
    var fibonacci:Fibonacci!
    override func setUp() {
        fibonacci = Fibonacci()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        fibonacci = nil
    }
    
    func testFibonacci()
    {
        XCTAssertEqual(fibonacci.getFabNumberWithSeries(with: 10),55,"Fail")
        XCTAssertEqual(fibonacci.getFabNumberFirstMethod(with: 9),34,"Fail")
        XCTAssertEqual(fibonacci.getFabNumberSecondMethod(with: 8),21,"Fail")
    }

}
