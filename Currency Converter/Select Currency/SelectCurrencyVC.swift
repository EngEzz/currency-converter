//
//  SelectCurrencyVC.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class SelectCurrencyVC: UIViewController,PresenterToViewSelectCurrencyProtocol {
    @IBOutlet weak var currencyPV: UIPickerView!
    weak var selectCurrencyDelegate:SelectCurrencyDelegate?
    var presenter: ViewToPresenterSelectCurrencyProtocol?
    var selectedRow = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeViews()
    }
    private func initializeViews()
    {
        self.presenter?.viewDidLoad()
    }
    func initializePickerView()
    {
        currencyPV.delegate = self
        currencyPV.dataSource = self
        currencyPV.reloadAllComponents()
        currencyPV.delegate?.pickerView?(currencyPV, didSelectRow: 0, inComponent: 0)
    }
    @IBAction private func done(_ sender: Any)
    {
        presenter?.selectCurrency()
    }
    @IBAction private func cancel(_ sender: Any)
    {
        presenter?.dismiss()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        presenter?.dismiss()
    }
    func reloadData() {
        initializePickerView()
    }
    

}
