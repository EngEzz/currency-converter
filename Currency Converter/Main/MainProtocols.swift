//
//  MainProtocols.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

protocol ViewToPresenterMainProtocol:class {
    var view: PresenterToViewMainProtocol? { get set }
    var interactor: PresenterToInteractorMainProtocol? { get set }
    var router: PresenterToRouterMainProtocol? { get set }
    var currencies: [String : Double]?  { get set }
    var baseRate:Double?  { get set }
    func viewDidLoad()
    func selectCurrency(tag:Int)
    func setSelectedCurrency(with tag : Int,currency:String)
    func showCalculationDialog(with currency : String , with rate:Double)
}

protocol PresenterToViewMainProtocol:class {
    func reloadData()
    func showErrorMessage(error:NSError)
    func showCurrency(currency:String)
    func calculateConversion(with currency : String , with rate:Double)
}
protocol PresenterToInteractorMainProtocol {
    var presenter: InteractorToPresenterMainProtocol? { get set }
    func getCurrencies()
    func getCurrencyBase(with tag : Int,currency:String)
}

protocol InteractorToPresenterMainProtocol:class {
    func fetchCurrenciesSuccess(currencies:[String:Double]?)
    func fetchCurrenciesFailure(error:NSError)
    func fetchCurrencyRate(with tag : Int,for currency:String , currencyRate:Double)
}
protocol PresenterToRouterMainProtocol {
    func showCurrencyDialog(tag:Int,currencies:[String:Double]?,view:PresenterToViewMainProtocol?)
    func showCalculationDialog(selectedCurrencies:[String:Any]?,currencies:[String:Double]?,view:PresenterToViewMainProtocol?)
    static func createMain()->UIViewController
}
