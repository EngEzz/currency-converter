//
//  CurrencyCalculationVC.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class CurrencyCalculationVC: UIViewController {

    @IBOutlet weak var conversionResultLb: UILabel!
    @IBOutlet weak var toCurrencyLb: UILabel!
    @IBOutlet weak var baseCurrencyLb: UILabel!
    var preseneter:ViewToPresenterCurrencyCalculationProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
    }
    func initializeViews()
    {
        preseneter?.viewDidLoad()
    }
    @IBAction func changeBaseCurrency(_ sender: Any)
    {
        preseneter?.selectCurrency()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        preseneter?.dismiss()
    }
}
extension CurrencyCalculationVC:PresenterToViewCurrencyCalculationProtocol{
    func showCurrencies(baseCurrency: String, userCurrency: String) {
        baseCurrencyLb.text = baseCurrency
        toCurrencyLb.text = userCurrency
    }
    
    func showConversionResult(conversion: Double) {
        conversionResultLb.text = conversion.description
    }
}
extension CurrencyCalculationVC:SelectCurrencyDelegate{
    func setSelectedCurrency(selectedCurrencyFor currency: String, tag: Int) {
        preseneter?.setSelectedCurrency(currency: currency)
    }
}
