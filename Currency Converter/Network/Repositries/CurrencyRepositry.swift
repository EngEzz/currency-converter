//
//  CurrencyRepositry.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
import RxSwift
class CurrencyRepositry:NSObject {
    
    func retrieveCurrencies()->Observable<Currency?>
    {
        return Observable.create{[weak self] observer in
            
            let _ = Connection.shared.retrieveData(urlName: AprService.currencies.rawValue, decoder: Currency.self).subscribe(onNext: {[weak self] data in
                guard let _ = self else {return}
                let _ = data?.success == true ? observer.onNext(data) : observer.onError(NSError(domain: data?.error?.info ?? "" , code: 0))
                },onError: {[weak self ] error in
                guard let _ = self else {return}
                observer.onError(error)
            })
            return Disposables.create()
        }
    }
}
