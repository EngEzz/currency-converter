//
//  SelectCurrencyProtocols.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
protocol ViewToPresenterSelectCurrencyProtocol:class {
    var view: PresenterToViewSelectCurrencyProtocol? { get set }
    var interactor: PresenterToInteractorSelectCurrencyProtocol? { get set }
    var router: PresenterToRouterSelectCurrencyProtocol? { get set }
    var selectedCurrency:String?{get set}
    func viewDidLoad()
    func numberOfRows()->Int
    func configureCurrency(row:Int)-> String?
    func selectCurrency(row:Int)
    func dismiss()
    func selectCurrency()
}

protocol PresenterToViewSelectCurrencyProtocol:class {
    func reloadData()
}
protocol PresenterToInteractorSelectCurrencyProtocol:class {
    var presenter: InteractorToPresenterSelectCurrencyProtocol? { get set }
    var currenciesRate: [String:Double]? { get set }
    func sortCurrencies()
}

protocol InteractorToPresenterSelectCurrencyProtocol:class {
    func fetchSortedCurrencies(currencies:[String])
}
protocol PresenterToRouterSelectCurrencyProtocol:class {
    var selectCurrencyDelegate:SelectCurrencyDelegate? { get set }
    var tag:Int? {get set}
    static func createSelectCurrency(with tag : Int ,with currencies:[String:Double]?,delegate:SelectCurrencyDelegate?)->UIViewController
    func selectCurrency(view:PresenterToViewSelectCurrencyProtocol?,for currency:String)
    func dismiss(view:PresenterToViewSelectCurrencyProtocol?)
}
