//
//  Anagram.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import Foundation
class Anagram {
    func isAnargam(firstWord:String,secondWord:String)->Bool
    {
        let firstWord = firstWord.lowercased()
        let secondWord = secondWord.lowercased()
        if firstWord.count != secondWord.count {return false}
        return getDicFromWord(word: firstWord) == getDicFromWord(word: secondWord)
    }
    private func getDicFromWord(word:String)->[Character:Int]
    {
        var wordDic:[Character:Int] = [:]
        for char in word
        {
            if let charCount = wordDic[char]
            {
                wordDic[char] = charCount + 1
            }
            else
            {
                wordDic[char] = 1
            }
        }
        return wordDic
    }
}
