

import Foundation
struct ServerError : Codable {
	let code : Int?
	let type : String?
	let info : String?

	enum CodingKeys: String, CodingKey {

		case code = "code"
		case type = "type"
		case info = "info"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		code = try values.decodeIfPresent(Int.self, forKey: .code)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		info = try values.decodeIfPresent(String.self, forKey: .info)
	}

}
