//
//  SelectCurrencyInteractor.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//


class SelectCurrencyInteractor:PresenterToInteractorSelectCurrencyProtocol {
    
    var presenter: InteractorToPresenterSelectCurrencyProtocol?
    var currenciesRate:[String:Double]?
    
    func sortCurrencies()
    {
        presenter?.fetchSortedCurrencies(currencies: currenciesRate?.getArray().sorted() ?? [])
    }
}
