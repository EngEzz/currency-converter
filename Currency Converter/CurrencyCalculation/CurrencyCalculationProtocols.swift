//
//  CurrencyCalculationProtocols.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

protocol ViewToPresenterCurrencyCalculationProtocol:class {
    var view: PresenterToViewCurrencyCalculationProtocol? { get set }
    var interactor: PresenterToInteractorCurrencyCalculationProtocol? { get set }
    var router: PresenterToRouterCurrencyCalculationProtocol? { get set }
    func viewDidLoad()
    func dismiss()
    func selectCurrency()
    func setSelectedCurrency(currency:String)
}

protocol PresenterToViewCurrencyCalculationProtocol:class {
    func showConversionResult(conversion:Double)
    func showCurrencies(baseCurrency:String,userCurrency:String)
}
protocol PresenterToInteractorCurrencyCalculationProtocol {
    var presenter: InteractorToPresenterCurrencyCalculationProtocol? { get set }
    var currencies:[String:Double]?{get set}
    var selectedCurrencies:[String:Any]?{get set}
    func calculateConversion(baseRate:Double)
    func retrieveCurrencies()
    func updateBase(currency:String)
}

protocol InteractorToPresenterCurrencyCalculationProtocol:class {
    func setCurrencies(withSelected selectedCurrencies:[String:Any]?,withCurrencies currencies:[String:Double]?)
    func setConversionResult(conversion:Double)
}
protocol PresenterToRouterCurrencyCalculationProtocol {
    static func createCurrencyCalculation(withSelected selectedCurrencies:[String:Any]?,withCurrencies currencies:[String:Double]?)->UIViewController
    func dismiss(view:PresenterToViewCurrencyCalculationProtocol?)
    func showCurrencyDialog(currencies:[String:Double]?,view:PresenterToViewCurrencyCalculationProtocol?)
}
