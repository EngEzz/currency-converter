//
//  MainVC.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
import RxSwift
class MainVC: UIViewController {

    @IBOutlet weak var selectCurrencyBt: UIButton!
    @IBOutlet weak var selectBaseBt: UIButton!
    @IBOutlet weak var baseCurrencyLb: UILabel!
    var presenter: ViewToPresenterMainProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeViews()
    }
    func initializeViews()
    {
        self.presenter?.viewDidLoad()
    }
    @IBAction private func selectBaseCurrency(_ sender: UIButton)
    {
        presenter?.selectCurrency(tag: sender.tag)
    }
    @IBAction private func selectCurrency(_ sender: UIButton)
    {
        presenter?.selectCurrency(tag: sender.tag)
    }
}
extension MainVC:PresenterToViewMainProtocol,SelectCurrencyDelegate{
    func calculateConversion(with currency : String , with rate:Double) {
        DispatchQueue.main.async{ self.presenter?.showCalculationDialog(with : currency, with : rate) }
    }
    
    func setSelectedCurrency(selectedCurrencyFor currency: String, tag: Int) {
        presenter?.setSelectedCurrency(with : tag ,currency: currency)
    }
    
    func removeBlurView() {
        self.view.viewWithTag(UIViewController.blurViewTag)?.removeFromSuperview()
    }
    func reloadData() {
        print("Success")
        selectBaseBt.isUserInteractionEnabled = true
    }
    func showErrorMessage(error: NSError) {
        print(error.localizedDescription)
    }
    func showCurrency(currency:String) {
        baseCurrencyLb.text = currency
        selectCurrencyBt.isUserInteractionEnabled = true
    }
}
