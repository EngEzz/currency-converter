//
//  SelectCurrencyPresenter.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//


class SelectCurrencyPresenter {
    var selectedCurrency: String?
    weak var view: PresenterToViewSelectCurrencyProtocol?
    var interactor: PresenterToInteractorSelectCurrencyProtocol?
    var router: PresenterToRouterSelectCurrencyProtocol?
    var currencies:[String] = []
    init(view:PresenterToViewSelectCurrencyProtocol,interactor:PresenterToInteractorSelectCurrencyProtocol,router:PresenterToRouterSelectCurrencyProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}
extension SelectCurrencyPresenter:ViewToPresenterSelectCurrencyProtocol{
    func numberOfRows() -> Int {
        return currencies.count
    }
    
    func configureCurrency(row: Int) -> String? {
        let currencyName = currencies[row]
        let currnecySymbol = currencyName.currencySymbol() ?? ""
        return "\(currencyName) \(currnecySymbol)"
    }
    func selectCurrency(row: Int) {
        selectedCurrency = currencies[row]
    }
    
    func dismiss() {
        router?.dismiss(view: view)
    }
    
    func selectCurrency() {
        router?.selectCurrency(view: view, for: selectedCurrency ?? "")
    }
    func viewDidLoad() {
        if let _ = interactor?.currenciesRate{
            interactor?.sortCurrencies()
        }
    }
}
extension SelectCurrencyPresenter:InteractorToPresenterSelectCurrencyProtocol{
    func fetchSortedCurrencies(currencies: [String]) {
        self.currencies = currencies
        view?.reloadData()
    }
}
