//
//  CurrencyCalculationInteractor.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//


class CurrencyCalculationInteractor {
    var presenter: InteractorToPresenterCurrencyCalculationProtocol?
    var currencies:[String:Double]?
    var selectedCurrencies:[String:Any]?
}
extension CurrencyCalculationInteractor:PresenterToInteractorCurrencyCalculationProtocol{
    func updateBase(currency: String) {
        selectedCurrencies?["baseCurrency"] = currency
        selectedCurrencies?["baseRate"] = currencies?[currency] ?? 0
        presenter?.setCurrencies(withSelected: selectedCurrencies, withCurrencies: currencies)
    }
    
    func calculateConversion(baseRate: Double) {
        let userCurrencyRate = selectedCurrencies?["userCurrencyRate"] as? Double ?? 0
        let conversion = userCurrencyRate / baseRate
        presenter?.setConversionResult(conversion: conversion)
    }
    
    func retrieveCurrencies() {
        presenter?.setCurrencies(withSelected: selectedCurrencies, withCurrencies: currencies)
    }
}
