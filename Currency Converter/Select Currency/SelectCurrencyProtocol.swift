//
//  SelectCurrencyProtocol.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

@objc protocol SelectCurrencyDelegate: NSObjectProtocol {
    func setSelectedCurrency(selectedCurrencyFor currency:String,tag:Int)
    @objc optional func removeBlurView()
}
