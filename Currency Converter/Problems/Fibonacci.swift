//
//  Fibonacci.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import Foundation
class Fibonacci{
    func getFabNumberWithSeries(with n:Int)->Int
    {
        var numbers:[Int] = []
        for number in 0...n-1
        {
            let fibNumber = number <= 1 ? number : numbers[number-1]+numbers[number-2]
            numbers.append(fibNumber)
        }
        print(numbers)
        return numbers[n-1] + numbers[n-2]
    }
    func getFabNumberFirstMethod(with n:Int)->Int
    {
        guard n > 1 else { return n }
        return getFabNumberFirstMethod(with:n-1) + getFabNumberFirstMethod(with:n-2)
    }
    func getFabNumberSecondMethod(with n:Int)->Int
    {
        var first = 0 , second = 1
        guard n > 1 else {return first}
        for _ in (1...n)
        {
            (first,second) = (first+second,first)
        }
        return first
    }
}
