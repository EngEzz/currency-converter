//
//  UIVIewController+Ex.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
    static let blurViewTag = 1000
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }
        return instantiateFromNib()
    }
    func addBlurView()
    {
        let blurView = UIView()
        blurView.backgroundColor = UIColor.black
        blurView.alpha = 0.5
        blurView.frame = self.view.bounds
        blurView.tag = UIViewController.blurViewTag
        self.view.addSubview(blurView)
    }
}
