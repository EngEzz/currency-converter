//
//  CurrencyCalculationRouter.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit
class CurrencyCalculationRouter {

}
extension CurrencyCalculationRouter:PresenterToRouterCurrencyCalculationProtocol{
    static func createCurrencyCalculation(withSelected selectedCurrencies:[String:Any]?,withCurrencies currencies:[String:Double]?)->UIViewController
    {
        let view = CurrencyCalculationVC.loadFromNib()
        let interactor = CurrencyCalculationInteractor()
        let router = CurrencyCalculationRouter()
        let presenter = CurrencyCalculationPresenter(view: view, interactor: interactor, router: router)
        interactor.presenter = presenter
        interactor.currencies = currencies
        interactor.selectedCurrencies = selectedCurrencies
        view.preseneter = presenter
        view.modalPresentationStyle = .overCurrentContext
        return view
    }
    func dismiss(view: PresenterToViewCurrencyCalculationProtocol?) {
        guard let vc = view as? UIViewController else {return}
        vc.dismiss(animated: true, completion: nil)
        vc.presentingViewController?.view.viewWithTag(UIViewController.blurViewTag)?.removeFromSuperview()
    }
    func showCurrencyDialog(currencies: [String : Double]?, view: PresenterToViewCurrencyCalculationProtocol?) {
        guard let vc = view as? UIViewController else {return}
        let selectCurrencyVC = SelectCurrencyRouter.createSelectCurrency(with : 0,with: currencies, delegate: vc as? SelectCurrencyDelegate)
        vc.present(selectCurrencyVC, animated: true, completion: nil)
    }
    
}
