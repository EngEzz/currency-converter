//
//  AppDelegate+Ex.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import Foundation
import UIKit
extension AppDelegate {
    internal func didFinishLaunching()
    {
        let mainVC = MainRouter.createMain()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController(rootViewController: mainVC)
        self.window?.makeKeyAndVisible()
    }
}
