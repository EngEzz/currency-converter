//
//  CurrencyCalculationPresenter.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//


class CurrencyCalculationPresenter {
    weak var view: PresenterToViewCurrencyCalculationProtocol?
    var interactor: PresenterToInteractorCurrencyCalculationProtocol?
    var router: PresenterToRouterCurrencyCalculationProtocol?
    var userCurrencyRate:Double?
    var selectedCurrencies:[String:Any]?
    var currencies:[String:Double]?
    init(view:PresenterToViewCurrencyCalculationProtocol,interactor:PresenterToInteractorCurrencyCalculationProtocol,router:PresenterToRouterCurrencyCalculationProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}
extension CurrencyCalculationPresenter:ViewToPresenterCurrencyCalculationProtocol{
    func setSelectedCurrency(currency: String) {
        interactor?.updateBase(currency: currency)
    }
    
    func selectCurrency() {
        router?.showCurrencyDialog(currencies: currencies, view: view)
    }
    
    func viewDidLoad() {
        if let _ = interactor?.selectedCurrencies{
            interactor?.retrieveCurrencies()
        }
    }
    func dismiss() {
        router?.dismiss(view: view)
    }
}
extension CurrencyCalculationPresenter:InteractorToPresenterCurrencyCalculationProtocol{
    func setCurrencies(withSelected selectedCurrencies: [String : Any]?, withCurrencies currencies: [String : Double]?) {
        self.currencies = currencies
        self.selectedCurrencies = selectedCurrencies
        view?.showCurrencies(baseCurrency: selectedCurrencies?["baseCurrency"] as? String ?? "" , userCurrency: selectedCurrencies?["userCurrency"] as? String ?? "")
        interactor?.calculateConversion(baseRate: selectedCurrencies?["baseRate"] as? Double ?? 0 )
    }
    func setConversionResult(conversion: Double) {
        view?.showConversionResult(conversion: conversion)
    }
}
