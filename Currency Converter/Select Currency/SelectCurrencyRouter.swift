//
//  SelectCurrencyRouter.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class SelectCurrencyRouter:PresenterToRouterSelectCurrencyProtocol {
    var tag: Int?
    var selectCurrencyDelegate: SelectCurrencyDelegate?
    static func createSelectCurrency(with tag : Int,with currencies: [String : Double]?,delegate:SelectCurrencyDelegate?) -> UIViewController {
        let view = SelectCurrencyVC.loadFromNib()
        let interactor = SelectCurrencyInteractor()
        let router = SelectCurrencyRouter()
        let presenter = SelectCurrencyPresenter(view: view,interactor: interactor,router: router)
        interactor.currenciesRate = currencies
        router.selectCurrencyDelegate = delegate
        router.tag = tag
        interactor.presenter = presenter
        view.presenter = presenter
        view.modalPresentationStyle = .overCurrentContext
        return view
    }
    func selectCurrency(view:PresenterToViewSelectCurrencyProtocol?,for currency: String) {
        dismiss(view: view)
        selectCurrencyDelegate?.setSelectedCurrency(selectedCurrencyFor: currency, tag: tag ?? 0)
    }
    
    func dismiss(view:PresenterToViewSelectCurrencyProtocol?) {
        selectCurrencyDelegate?.removeBlurView?()
        guard let vc = view as? UIViewController else {return}
        vc.dismiss(animated: true, completion: nil)
    }
    

}
