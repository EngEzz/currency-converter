//
//  MainInteractor.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class MainInteractor{
    weak var presenter:InteractorToPresenterMainProtocol?
    private let currencyRepo = CurrencyRepositry()
    private var currenciesRate:[String:Double]?
    
    
}
extension MainInteractor:PresenterToInteractorMainProtocol{
    func getCurrencies() {
        let _ = currencyRepo.retrieveCurrencies().subscribe(onNext: {[weak self] data in
            self?.currenciesRate = data?.rates
            self?.presenter?.fetchCurrenciesSuccess(currencies: self?.currenciesRate)
            },onError: {[weak self] error in
                self?.presenter?.fetchCurrenciesFailure(error: NSError(domain: error.localizedDescription, code: 0))
        })
    }
    func getCurrencyBase(with tag : Int ,currency: String) {
        presenter?.fetchCurrencyRate(with: tag, for:currency , currencyRate: currenciesRate?[currency] ?? 0)
    }
}
