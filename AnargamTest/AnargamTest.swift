//
//  AnargamTest.swift
//  AnargamTest
//
//  Created by ahmed ezz on 9/15/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import XCTest
@testable import Currency_Converter
class AnargamTest: XCTestCase {
    
    var anargam:Anagram!
    override func setUp() {
        continueAfterFailure = false
        anargam = Anagram()
    }
    override func tearDown() {
        anargam = nil
    }
    func testAnargam()
    {
        XCTAssertEqual(anargam.isAnargam(firstWord: "AbC", secondWord: "cab"),true,"Words not anargams")
        XCTAssertEqual(anargam.isAnargam(firstWord: "123", secondWord: "321"),true,"Words not anargams")
        XCTAssertEqual(anargam.isAnargam(firstWord: "anargam", secondWord: "magranm"),false,"Words not anargams")
        XCTAssertEqual(anargam.isAnargam(firstWord: "12345", secondWord: "123456"),false,"Words not anargams")
        XCTAssertEqual(anargam.isAnargam(firstWord: "test", secondWord: "Est"),false,"Words not anargams")
    }
    
}
