//
//  MainRouter.swift
//  Currency Converter
//
//  Created by ahmed ezz on 9/14/20.
//  Copyright © 2020 ahmed ezz. All rights reserved.
//

import UIKit

class MainRouter:PresenterToRouterMainProtocol {
    static func createMain()->UIViewController
    {
        let view = MainVC.loadFromNib()
        let router = MainRouter()
        let interactor = MainInteractor()
        let presenter = MainPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        return view
    }
    func showCurrencyDialog(tag:Int,currencies:[String:Double]?,view:PresenterToViewMainProtocol?) {
        guard let vc = view as? UIViewController else {return}
        let selectCurrencyVC = SelectCurrencyRouter.createSelectCurrency(with : tag,with: currencies, delegate: vc as? SelectCurrencyDelegate)
        vc.addBlurView()
        vc.present(selectCurrencyVC, animated: true, completion: nil)
    }
    func showCalculationDialog(selectedCurrencies: [String : Any]?, currencies: [String : Double]?, view: PresenterToViewMainProtocol?) {
        guard let vc = view as? UIViewController else {return}
        let currencyCalculationVC = CurrencyCalculationRouter.createCurrencyCalculation(withSelected: selectedCurrencies, withCurrencies: currencies)
        vc.addBlurView()
        vc.present(currencyCalculationVC, animated: false, completion: nil)
    }
}

